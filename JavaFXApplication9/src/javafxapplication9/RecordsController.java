/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication9;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.scene.control.Label;/**
 * FXML Controller class
 *
 * @author kdegb_000
 */
public class RecordsController implements Initializable {

    @FXML
    private TableColumn<user, String> Nmae;
    @FXML
    private TableColumn<user, String> Number;
    @FXML
    private TableColumn<user, String> Adress;
    @FXML
    private TableColumn<user, String> QtyTables;
    @FXML
    private TableColumn<user, String> Canopies;
    @FXML
    private TableColumn<user, String> QtyChairs;
    @FXML
    private TableColumn<user, String> Duration;
    @FXML
    private TableColumn<user, String> Birthdate;
    @FXML
    private Tab TabSearch;
    @FXML
    private Button btnSearch;
    @FXML
    private TextField txtSearch;
    @FXML
    private Button btnMainMenu;
    @FXML
    private Tab tabManage;
    @FXML
    private TableColumn<user, String> Nmae1;
    @FXML
    private TableColumn<user, String> Number1;
    @FXML
    private TableColumn<user, String> Adress1;
    @FXML
    private TableColumn<user, String> QtyTables1;
    @FXML
    private TableColumn<user, String> Canopies1;
    @FXML
    private TableColumn<user, String> QtyChairs1;
    @FXML
    private TableColumn<user, String> Duration1;
    @FXML
    private TableColumn<user, String> Birthdate1;
    @FXML
    private TableView<user> table;
    
    private ObservableList<user> data;
    
    private DbConnection dc;
    @FXML
    private TableView<user> table2;
    @FXML
    private Label warn;
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtNumber;
    @FXML
    private TextField txtAdress;
    @FXML
    private TextField txtTables;
    @FXML
    private TextField txtCanopies;
    @FXML
    private TextField txtChairs;
    @FXML
    private TextField txtDuration;
    
    private static int index;
    @FXML
    private DatePicker datepick;
    @FXML
    private Label lblwarn1;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
       try {
            dc = new DbConnection();
            Connection conn = dc.con();
            data = FXCollections.observableArrayList();

            ResultSet rs = conn.createStatement().executeQuery("select * from record5");
            while (rs.next()) {
              //  data.add(new user(rs.getString("user"), rs.getString("email"), rs.getInt("money")));
                
                data.add(new user(rs.getString("name"),rs.getInt("number"),rs.getString("adress"),
                        rs.getInt("qtychairs"),rs.getInt("qtytables"),rs.getInt("qtycanopies"),
                        rs.getInt("duartion"),rs.getString("dot")));
                
                System.out.println(data);
                System.out.println(rs.getString("name"));
            }

        } catch (SQLException ex) {
            System.out.println("Error \n" + ex);
        }

        Nmae1.setCellValueFactory(new PropertyValueFactory<>("name"));
        Number1.setCellValueFactory(new PropertyValueFactory<>("number"));
        Adress1.setCellValueFactory(new PropertyValueFactory<>("adress"));
        QtyTables1.setCellValueFactory(new PropertyValueFactory<>("tables"));
        QtyChairs1.setCellValueFactory(new PropertyValueFactory<>("chairs"));
        Canopies1.setCellValueFactory(new PropertyValueFactory<>("canopies"));
        Duration1.setCellValueFactory(new PropertyValueFactory<>("duration"));
        Birthdate1.setCellValueFactory(new PropertyValueFactory<>("date"));

        table.setItems(null);
        table2.setItems(data);
        
        
        
      

    }
    
/**
 * allows user to search for people by name
 * @param event 
 */

    @FXML
    private void search(ActionEvent event) {
         try {
            data = FXCollections.observableArrayList();
            dc = new DbConnection();
            Connection conn = dc.con();

            ResultSet rs = conn.createStatement().executeQuery("select * from record5 where name = '" + txtSearch.getText() + "'");
            if (rs.next()) {
                warn.setText("");
                data.add(new user(rs.getString("name"),rs.getInt("number"),rs.getString("adress"),
                        rs.getInt("qtychairs"),rs.getInt("qtytables"),rs.getInt("qtycanopies"),
                        rs.getInt("duartion"),rs.getString("dot")));
                //placec in user details object
        Nmae.setCellValueFactory(new PropertyValueFactory<>("name"));
        Number.setCellValueFactory(new PropertyValueFactory<>("number"));
        Adress.setCellValueFactory(new PropertyValueFactory<>("adress"));
        QtyTables.setCellValueFactory(new PropertyValueFactory<>("tables"));
        QtyChairs.setCellValueFactory(new PropertyValueFactory<>("chairs"));
        Canopies.setCellValueFactory(new PropertyValueFactory<>("canopies"));
        Duration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        Birthdate.setCellValueFactory(new PropertyValueFactory<>("date"));

        table.setItems(null);
        table.setItems(data);
            } else {
                warn.setText("User not found");
            }

        } catch (SQLException ex) {
            warn.setText("User not found");
        }

    }

    /**
     * allows user to go back to the main menu
     * @param event 
     */
    @FXML
    private void MainMenu(ActionEvent event) {
        try {
            Parent questionPage = FXMLLoader.load(getClass().getResource("home.fxml"));
            Scene three = new Scene(questionPage);
            Stage quest = (Stage) ((Node) event.getSource()).getScene().getWindow();
            quest.setScene(three);
            quest.setTitle("Home");
            quest.show();
        } catch (IOException ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
/*
    @FXML
    private void delete(ActionEvent event) {
      /*    String name = userTable.getSelectionModel().getSelectedItem().getUser();
        int index = userTable.getSelectionModel().getSelectedIndex();

        try {
            userTable.getItems().remove(index);
            dc = new DbConnection();
            Connection conn = dc.con();
            Statement sm = conn.createStatement();
            sm.execute("delete from user_info where user = '" + name + "'");

        } catch (SQLException e) {
            //database error
            e.printStackTrace();
        }

    }*/

   /* @FXML
    private void edit(ActionEvent event) {
        
          
        
   
        
       
       );

    }
    */
/**
 * allows a user to delete a row
 * @param event 
 */
    @FXML
    private void delete(ActionEvent event) {
        
    String name = table2.getSelectionModel().getSelectedItem().getName();
        int index1;
        index1 = table2.getSelectionModel().getSelectedIndex();

        try {
            table2.getItems().remove(index1);
            dc = new DbConnection();
            Connection conn = dc.con();
            Statement sm = conn.createStatement();
            sm.execute("delete from record5 where name = '" + name + "'");

        } catch (SQLException e) {
        //database error

        }    
    }
/**
 * aloows user to update a row
 * @param event 
 */
    @FXML
    private void edit(ActionEvent event) {
         String name = table2.getSelectionModel().getSelectedItem().getName();
        
     
        
       
        
        index = table2.getSelectionModel().getSelectedIndex();
        
        user a =new user(null,null,null);
        
        //dob = twoDOB.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
           //  Integer.parseInt(txtNumber.getText()) ;
              
         if(isInteger(txtName.getText())|!isInteger(txtNumber.getText())|
                  
                  isInteger(txtAdress.getText())|!isInteger(txtChairs.getText())|!isInteger(txtTables.getText())|
                  
                  !isInteger(txtCanopies.getText())|!isInteger(txtDuration.getText())|datepick.getValue() ==null
                  
                  ){
          lblwarn1.setText("check your formats");
          
          }      
         else{   
           //   Integer.toString(a2);
        a.updateRecords(
               
            "record5","name","number","adress",
            
            "qtychairs","qtytables","qtycanopies","duartion","dot",
            
            txtName.getText(),Integer.parseInt(txtNumber.getText()),txtAdress.getText(),
            
            Integer.parseInt(txtChairs.getText()),Integer.parseInt(txtTables.getText()),
            
            Integer.parseInt(txtCanopies.getText()),
            
            Integer.parseInt(txtDuration.getText()),datepick.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),"name",
              table2.getSelectionModel().getSelectedItem().getName()
        );
        
         
        
               txtName.clear();
    
    txtNumber.clear();
   
     txtAdress.clear();
    
    txtTables.clear();
    
    txtCanopies.clear();
    
    txtChairs.clear();
    
    txtDuration.clear();
    
        lblwarn1.setText("");
        datepick.getEditor().clear();
        this.refresh(event);
    }
    }
    
    /**
     * allows user to select a particular row
     * @param event 
     */
    @FXML
    private void select(ActionEvent event) {
        
           String name = table2.getSelectionModel().getSelectedItem().getName();
        
         Integer number =table2.getSelectionModel().getSelectedItem().getNumber();
        
        String adress =table2.getSelectionModel().getSelectedItem().getAdress();
        
        Integer tables =table2.getSelectionModel().getSelectedItem().getTables();
        
        Integer canopies =table2.getSelectionModel().getSelectedItem().getCanopies();
        
        Integer chairs = table2.getSelectionModel().getSelectedItem().getChairs();
        
        Integer duration =table2.getSelectionModel().getSelectedItem().getDuration();
        
        String formattedValue = table2.getSelectionModel().getSelectedItem().getDate();
        
      //  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        
       // String formattedValue = (datepick.getValue()).format(formatter);
        
      //  String dot =table2.getSelectionModel().getSelectedItem().getDate();
        
       
       
       
        
         txtName.setText(name);
    
    txtNumber.setText(Integer.toString(number));
   
     txtAdress.setText(adress);
    
    txtTables.setText(Integer.toString(tables));
    
    txtCanopies.setText(Integer.toString(canopies));
    
    txtChairs.setText(Integer.toString(chairs));
    
    txtDuration.setText(Integer.toString(duration));
    
    datepick.setValue(LocalDate.parse(formattedValue));
    }

    /**
     * refreshes the table
     * @param event 
     */
    private void refresh(ActionEvent event) {
        
         try {
            dc = new DbConnection();
            Connection conn = dc.con();
            data = FXCollections.observableArrayList();

            ResultSet rs = conn.createStatement().executeQuery("select * from record5");
            while (rs.next()) {
              //  data.add(new user(rs.getString("user"), rs.getString("email"), rs.getInt("money")));
                
                 data.add(new user(rs.getString("name"),rs.getInt("number"),rs.getString("adress"),
                        rs.getInt("qtychairs"),rs.getInt("qtytables"),rs.getInt("qtycanopies"),
                        rs.getInt("duartion"),rs.getString("dot")));
                
                System.out.println(data);
                System.out.println(rs.getString("name"));
            }

        } catch (SQLException ex) {
            System.out.println("Error \n" + ex);
        }

        Nmae1.setCellValueFactory(new PropertyValueFactory<>("name"));
        Number1.setCellValueFactory(new PropertyValueFactory<>("number"));
        Adress1.setCellValueFactory(new PropertyValueFactory<>("adress"));
        QtyTables1.setCellValueFactory(new PropertyValueFactory<>("tables"));
        QtyChairs1.setCellValueFactory(new PropertyValueFactory<>("chairs"));
        Canopies1.setCellValueFactory(new PropertyValueFactory<>("canopies"));
        Duration1.setCellValueFactory(new PropertyValueFactory<>("duration"));
        Birthdate1.setCellValueFactory(new PropertyValueFactory<>("date"));

        table.setItems(null);
        table2.setItems(data);
        
    }
/**
 * add user to table
 * @param event 
 */
    @FXML
    private void add(ActionEvent event) {
        
          user a =new user(null,null,null);
          
          if (a.checkInput(txtName.getText())){
          
              lblwarn1.setText("User already exists");
          
          }
          
          else if(
                  txtName.getText().equals("")|txtNumber.getText().equals("")|txtAdress.getText().equals("")|
                  
                  txtChairs.getText().equals("")|txtTables.getText().equals("")|
                  
                  txtCanopies.getText().equals("")|txtDuration.getText().equals("")|
                  
                  datepick.getValue() ==null
                  ){
           lblwarn1.setText("enter all records");
          }
          else if(isInteger(txtName.getText())|!isInteger(txtNumber.getText())|
                  
                  isInteger(txtAdress.getText())|!isInteger(txtChairs.getText())|!isInteger(txtTables.getText())|
                  
                  !isInteger(txtCanopies.getText())|!isInteger(txtDuration.getText())
                  
                  ){
          lblwarn1.setText("check your formats");
          
          }
          else{
              int a2 = Integer.parseInt(txtNumber.getText()) ;
              System.out.println(a2);
              String a22 =Integer.toString(a2);
              System.out.println(a22);
              int a3=Integer.parseInt(txtChairs.getText());
              String a33 =Integer.toString(a3);
              int a4 = Integer.parseInt(txtTables.getText());
              String a44 = Integer.toString(a4);
              int a5 = Integer.parseInt(txtCanopies.getText());
              String a55 = Integer.toString(a5);
              int a6 =Integer.parseInt( txtDuration.getText());
              System.out.println(a6);
              String a66 =Integer.toString(a6);
              a.addRecord(txtName.getText(),a2,txtAdress.getText(),a3,a4,a5,
                      a6,datepick.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
              
              //  a.addRecord("k",3,"k",3,3,3,3,null);
        
     txtName.clear();
    
    txtNumber.clear();
   
     txtAdress.clear();
    
    txtTables.clear();
    
    txtCanopies.clear();
    
    txtChairs.clear();
    
    txtDuration.clear();
    
    lblwarn1.setText("");
    
    datepick.getEditor().clear();
    
        this.refresh(event);
        
          }
    }
    /**
     * check if value is intgeger
     * @param s
     * @return true if it is
     */
    public static boolean isInteger(String s) {
    try { 
        Integer.parseInt(s); 
    } catch(NumberFormatException e) { 
        return false; 
    } catch(NullPointerException e) {
        return false;
    }
    // only got here if we didn't return false
    return true;
}
    

    
}


    
    
   
     
    
    
    
  