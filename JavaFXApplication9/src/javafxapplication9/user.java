/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication9;

/**
 *
 * @author kdegb_000
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import static javafxapplication9.database.update;



public class user {
    
 database db;   
  
  final StringProperty id;
    
  final StringProperty password;
  
  final StringProperty name;
  
  final IntegerProperty number;
  
  final StringProperty adress;
  
  final IntegerProperty chairs;
  
  final IntegerProperty tables;
  
  final IntegerProperty canopies;
  
  final IntegerProperty duration;
  
  final StringProperty date ;
  
    /**
     *contructor
     * @param name
     * @param number
     * @param adress
     * @param chairs
     * @param tables
     * @param canopies
     * @param duration
     * @param date
     */
    public user(String name,Integer number,String adress,Integer chairs,Integer tables,
          Integer canopies,Integer duration,String date){
 
 this.db = new database();
  
  this.id=null;
  
  this.password= null;
  
  this.name = new SimpleStringProperty (name);
  
  this.number =new SimpleIntegerProperty (number);
  
  this.adress =new SimpleStringProperty (adress);
  
  this.chairs =new SimpleIntegerProperty (chairs);
  
  this.tables =new SimpleIntegerProperty (tables);
  
  this.canopies =new SimpleIntegerProperty (canopies);
  
  this.duration = new SimpleIntegerProperty (duration);
  
  this.date =  new SimpleStringProperty (date); 
 
 
 }
  /**
   * contrutor
   * @param id
   * @param password
   * @param date 
   */
  public user(String id,String password,String date){
        this.db = new database();
  
  this.id=new SimpleStringProperty (id);
  
  this.password= new SimpleStringProperty (password);
  
  this.name =null;
  
  this.number =null;
  
  this.adress =null;
  
  this.chairs =null;
  
  this.tables =null;
  
  this.canopies =null;
  
  this.duration = null;
  
  this.date = new SimpleStringProperty (date); 
 
  }
  /**
   * constructor for login purposes
   * @param id
   * @param password 
   */
  //for login purposes
   public user(String id ,String password){
  
      this.db =new database();
       
      this.id=new SimpleStringProperty (id);
  
  this.password=new SimpleStringProperty (password);
  
  this.name =null;
  
  this.number = null;
  
  this.adress = null;
  
  this.chairs = null;
  
  this.tables = null;
  
  this.canopies =null;
  
  this.duration = null;
  
  this.date =  null; 
 
  }
  
 
  /**
   * lets the user add a record to table
   * @param name
   * @param number
   * @param adress
   * @param chairs
   * @param tables
   * @param canopies
   * @param duration
   * @param dot 
   */
  public void addRecord(String name,Integer number,String adress,Integer chairs,Integer tables,
          Integer canopies,Integer duration,String dot){
  
     try {
         database.insert("record5","name","number","adress","qtychairs","qtytables","qtycanopies","duartion","dot"
                 ,name,number,adress,chairs,tables,canopies,duration,dot);
     } catch (InstantiationException ex) {
         Logger.getLogger(user.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  
  /**
   * let the user update table
   * @param records
   * @param name
   * @param number
   * @param adress
   * @param chairs
   * @param tables
   * @param canopies
   * @param duration
   * @param dot
   * @param nameValue
   * @param numberValue
   * @param adressValue
   * @param chairsValue
   * @param tablesValue
   * @param canopiesValue
   * @param durationValue
   * @param dotValue
   * @param clauseAttribute
   * @param clauseAttributeValue 
   */
  public void  updateRecords(
               
            String records,String name,String number,String adress,
            
            String chairs,String tables,String canopies,String duration,String dot,
            
            String nameValue,Integer numberValue,String adressValue,
            
            Integer chairsValue,Integer tablesValue,Integer canopiesValue,
            
            Integer durationValue,String dotValue,String clauseAttribute,String clauseAttributeValue
       ){
  
  try { 
            database.update(
               
            records,name,number,adress,
            
            chairs,tables,canopies,duration,dot,
            
            nameValue,numberValue,adressValue,
            
            chairsValue,tablesValue,canopiesValue,
            
            durationValue,dotValue,clauseAttribute,clauseAttributeValue
       );
         } catch (InstantiationException ex) {
                Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
         }
  
  }
  /**
   * allows yser to login
   * @param id
   * @param pass
   * @return 
   */
  public boolean login(String id,String pass){
      
     return db.authenticate(id,pass,"admin");
  
  }
  
  /**
   * checks if input is in database 
   * @param name
   * @return true if it is true
   */
  public boolean checkInput(String name){
  
  return db.checkInput(name);
  }
  
  
  public String getName() {
        return name.get();
    }
  public Integer getNumber() {
        return number.get();
    }
  public String getAdress() {
        return adress.get();
    }
  public Integer getTables() {
        return tables.get();
    }
  public Integer getCanopies() {
        return canopies.get();
    }
  public Integer getChairs() {
        return chairs.get();
    }
  public Integer getDuration() {
        return duration.get();
    }
  public String getDate() {
        return date.get();
    }
  
  public StringProperty nameProperty() {
        return name;
    }
  public IntegerProperty numberProperty() {
        return number;
    }
  public StringProperty adressProperty() {
        return adress;
    }
  public IntegerProperty tablesProperty() {
        return tables;
    }
  public IntegerProperty canopiesProperty() {
        return canopies;
    }
  public IntegerProperty chairsProperty() {
        return chairs;
    }
  public IntegerProperty durationProperty() {
        return duration;
    }
  public StringProperty dateProperty() {
        return date;
    }
  
  
  
 
  
}
