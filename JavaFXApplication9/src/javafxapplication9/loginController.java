/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication9;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author kdegb_000
 */
public class loginController implements Initializable {

    @FXML
    private PasswordField lblPassword;
    @FXML
    private Button btnLogin;
    @FXML
    private TextField txtUsername;
    @FXML
    private Label lblwarn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
/**
 * allows the user to login in to app
 * @param event 
 */
    @FXML
    private void login(ActionEvent event) {
       user a =new user(txtUsername.getText(),lblPassword.getText());
       
       if (a.login(txtUsername.getText(),lblPassword.getText())){
       try {
            Parent questionPage = FXMLLoader.load(getClass().getResource("home.fxml"));
            Scene three = new Scene(questionPage);
            Stage quest = (Stage) ((Node) event.getSource()).getScene().getWindow();
            quest.setScene(three);
            quest.setTitle("Home");
            quest.show();
        } catch (IOException ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       else{
       lblwarn.setText("Wrong details");
       }
    
}
}
