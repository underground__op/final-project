/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication9;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author kdegb_000
 */
public class HomeController implements Initializable {

    @FXML
    private Button btnRecords;
    @FXML
    private Button btnlogout;
    @FXML
    private Button btnPrint;
    @FXML
    private Label lblNoti;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    /**
     * this prints the file
     * @param event if clicked on it activates
     */
    @FXML
    private void Transactions(ActionEvent event) {
        
      //label to say printed or something went wrong
      
       @SuppressWarnings("UnusedAssignment")
  java.sql.Connection conn = null;
   
       String name =null;
    int number = 0;
    String adress =null;
    int chairs =0;
    int tables =0;
    int canopies =0;
    int duration =0;
    String date =null;  
    
    int priceChairs = 5;
    
    int priceTables = 8;
    
    int priceCanopies = 12;
    
    int totalChairs = priceChairs*chairs;
    
    int totalTables = priceTables*tables;
    
    int totalCanopies = priceCanopies*canopies;
    
    int total = totalChairs + totalTables + totalCanopies;
    
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
             
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/final?user=root&password=root");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println(e);
            System.exit(0);
        }

   try {
      try (java.sql.Statement s = conn.createStatement();
       java.sql.ResultSet r = s.executeQuery("SELECT * from record5"); 
              
             // PrintWriter pr = new PrintWriter(new FileWriter(
             //         "C:\\Users4\\kdegb_000\\Documents\\Final project\\JavaFXApplication9\\src\\javafxapplication9\\analytics.txt"))) {
          
           PrintWriter dr = new PrintWriter(new FileWriter("analytics.txt"))){
           
          while (r.next()) {
              
              /*System.out.println (    r.getString("titel") + " " +    r.getString("interpret") +
              " " +    r.getString("jahr") ); */
              name=r.getString("name");
              
         //     pr.println("The user name is: "+name);
              
             dr.println("The user name is: "+name);

              
              number=r.getInt("number");
              
          //    pr.println("Their number is : "+number);
              
              dr.println("Their number is : "+number);
              
              adress = r.getString("adress");
              
              date = r.getString("dot");
              
           //   pr.println("Their adress is: "+adress+" and their birthdate is "+date);
              
              dr.println("Their adress is: "+adress+" and their birthdate is "+date);
              
              chairs=r.getInt("qtychairs");
              
           //   pr.println("They orderd "+chairs+" number of chairs and the price of chair is "+priceChairs+
          //    "so the total amount for chair is "+ priceChairs*chairs
          //    );
              
              dr.println("They orderd "+chairs+" number of chairs and the price of chair is "+priceChairs+
              "so the total amount for chair is "+ priceChairs*chairs
              );
              
              tables=r.getInt("qtytables");
              
          //    pr.println("They orderd "+tables+" number of chairs and the price of chair is "+priceTables+
          //    " so the total amount for chair is "+ tables*priceTables
          //    );
              
              dr.println("They orderd "+tables+" number of chairs and the price of chair is "+priceTables+
              " so the total amount for chair is "+ tables*priceTables
              );
              
              
              canopies =r.getInt("qtycanopies");
              
         //     pr.println("They orderd "+canopies+" number of chairs and the price of chair is "+priceCanopies+
         //     " so the total amount for chair is "+ canopies*priceCanopies
         //     );
              
              dr.println("They orderd "+canopies+" number of chairs and the price of chair is "+priceCanopies+
              " so the total amount for chair is "+ canopies*priceCanopies
              );
              
              duration=r.getInt("duartion");
              
              int t = ((priceChairs*chairs)+(tables*priceTables) +(canopies*priceCanopies));
              
           //   pr.println("They have a duartion of "+duration+" hours and their total cost is "+t) ;
              
           //   pr.println("  ");
          //      pr.println("  ");
                
                  dr.println("They have a duartion of "+duration+" hours and their total cost is "+t) ;
              
              dr.println("  ");
                dr.println("  ");
                
          }
        
       //   pr.close();
          dr.close();
      }    catch (IOException ex) {
               Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
           }
      lblNoti.setText("done printing");
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.exit(0);
        }
   
    }
/**
 * this takes the user to the records page
 * @param event if clicked on it activates
 */
    @FXML
    private void Records(ActionEvent event) {
        
        try {
            Parent questionPage = FXMLLoader.load(getClass().getResource("Records.fxml"));
            Scene three = new Scene(questionPage);
            Stage quest = (Stage) ((Node) event.getSource()).getScene().getWindow();
            quest.setScene(three);
            quest.setTitle("Records");
            quest.show();
        } catch (IOException ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
/**
 * This helps the user logout
 * @param event if clicked on it activates
 */
    @FXML
    private void Logout(ActionEvent event) {
        
        try {
            Parent questionPage = FXMLLoader.load(getClass().getResource("login.fxml"));
            Scene three = new Scene(questionPage);
            Stage quest = (Stage) ((Node) event.getSource()).getScene().getWindow();
            quest.setScene(three);
            quest.setTitle("login");
            quest.show();
        } catch (IOException ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
