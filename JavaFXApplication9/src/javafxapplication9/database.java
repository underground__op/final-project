/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication9;

/**
 *
 * @author kdegb_000
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author kdegb_000
 */




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.*; 
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kdegb_000
 */
public class database {
    
    /**
     *insert into database
     * @param tableName
     * @param attribute1
     * @param attribute2
     * @param attribute3
     * @param attribute4
     * @param attribute5
     * @param attribute6
     * @param attribute7
     * @param attribute8
     * @param attribute1Value
     * @param attribute2Value
     * @param attribute3Value
     * @param attribute4Value
     * @param attribute5Value
     * @param attribute6Value
     * @param attribute7Value
     * @param attribute8Value
     * @throws java.lang.InstantiationException
     */
    public static void insert(String tableName,String attribute1,String attribute2,String attribute3,
            
            String attribute4,String attribute5,String attribute6,String attribute7,String attribute8,
            
            String attribute1Value,Integer attribute2Value,String attribute3Value,
            
            Integer attribute4Value,Integer attribute5Value,Integer attribute6Value,
            
            Integer attribute7Value,String attribute8Value)
            throws InstantiationException
  {
  
    try{ 
   Class.forName("com.mysql.jdbc.Driver").newInstance();  
  
    Connection conn = java.sql.DriverManager.getConnection(  
            "jdbc:mysql://localhost/final?user=root&password=root"); 
    
    String a ="Insert Into ";
    
    String b= tableName;
    
    String c =" set ";
    
    String d = attribute1+ "=?,";
    
    String e =attribute2+ "=?,";
    
    String f =attribute3+ "=?,";
    
    String g =attribute4+ "=?,";
    
    String h =attribute5+ "=?,";
    
    String i =attribute6+ "=?,";
    
    String j =attribute7+ "=?,";
    
    String k =attribute8+ "=?";
    
    
   PreparedStatement p= conn.prepareStatement(a+b+c+d+e+f+g+h+i+j+k);    
  //  p.setString(1, tableValue);   
    p.setString(1, attribute1Value );    
    p.setInt(2, attribute2Value);   
    p.setString(3,attribute3Value) ;
    p.setInt(4,attribute4Value) ;
    p.setInt(5,attribute5Value) ;
    p.setInt(6,attribute6Value) ;
    p.setInt(7,attribute7Value) ;
    p.setString(8,attribute8Value) ;
    p.execute();  
//use execute if no results expected back   
  }catch(SQLException e){         System.out.println("Error"+e.toString());         
 }       catch (IllegalAccessException | ClassNotFoundException ex) { 
             Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
         }
  
} 
  
    /**
     *check if user is in database 
     * @param userId
     * @param userPass
     * @param tableName
     * @return
     */
    public boolean authenticate(String userId,String userPass,String tableName){
        
    boolean statement =false;
    @SuppressWarnings("UnusedAssignment")
    java.sql.Connection conn = null;
    
    String a =tableName;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
             
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/final?user=root&password=root");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println(e);
            System.exit(0);
        }

   try {
        try (java.sql.Statement s = conn.createStatement(); 
                java.sql.ResultSet r = s.executeQuery("SELECT * FROM"+" "+a)) {

            while (r.next()) {
                
                /*System.out.println (    r.getString("titel") + " " +    r.getString("interpret") +
                " " +    r.getString("jahr") ); */
                if (r.getString("id").equals(userId) && r.getString("pass").equals(userPass)) {
                    statement=true;
                }
            }
            
            
            
        }
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.exit(0);
        }
   return statement;
}
      public boolean checkInput(String name){
        
    boolean statement =false;
    @SuppressWarnings("UnusedAssignment")
    java.sql.Connection conn = null;
    
    String a ="record5";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
             
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/final?user=root&password=root");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println(e);
            System.exit(0);
        }

   try {
        try (java.sql.Statement s = conn.createStatement(); 
                java.sql.ResultSet r = s.executeQuery("SELECT * FROM"+" "+a)) {

            while (r.next()) {
                
                /*System.out.println (    r.getString("titel") + " " +    r.getString("interpret") +
                " " +    r.getString("jahr") ); */
                if (r.getString("name").equals(name)) {
                    statement=true;
                }
            }
            
            
            
        }
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.exit(0);
        }
   return statement;
}
  
  //for select all command
 // public String[] getAll();
    
    /**
     *get one attribute from table
     * @param Id
     * @param attribute
     * @return
     */
    public String getTablenAttribute(String Id,String attribute){
  
  @SuppressWarnings("UnusedAssignment")
  java.sql.Connection conn = null;
    String statement =null;
    String a =Id;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
             
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/final?user=root&password=root");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println(e);
            System.exit(0);
        }

   try {
      try (java.sql.Statement s = conn.createStatement(); java.sql.ResultSet r = s.executeQuery("SELECT" +" "+ attribute  +" "+ "FROM question where id ="+a)) {
          
          while (r.next()) {
              
              /*System.out.println (    r.getString("titel") + " " +    r.getString("interpret") +
              " " +    r.getString("jahr") ); */
              statement=r.getString(attribute);
          }
          
          
          
      }
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
            System.exit(0);
        }
   return statement;
  
  
  }
   /**
    * update values in database
    * @param tableName
    * @param attribute1
    * @param attribute2
    * @param attribute3
    * @param attribute4
    * @param attribute5
    * @param attribute6
    * @param attribute7
    * @param attribute8
    * @param attribute1Value
    * @param attribute2Value
    * @param attribute3Value
    * @param attribute4Value
    * @param attribute5Value
    * @param attribute6Value
    * @param attribute7Value
    * @param attribute8Value
    * @param clauseAttribute
    * @param clauseAttributeValue
    * @throws InstantiationException 
    */
       public static void  update(
               
            String tableName,String attribute1,String attribute2,String attribute3,
            
            String attribute4,String attribute5,String attribute6,String attribute7,String attribute8,
            
            String attribute1Value,Integer attribute2Value,String attribute3Value,
            
            Integer attribute4Value,Integer attribute5Value,Integer attribute6Value,
            
            Integer attribute7Value,String attribute8Value,String clauseAttribute,String clauseAttributeValue
       )
            throws InstantiationException
  {
  
    try{ 
   Class.forName("com.mysql.jdbc.Driver").newInstance();  
  
    Connection conn = java.sql.DriverManager.getConnection(  
            "jdbc:mysql://localhost/final?user=root&password=root"); 
    
    String a ="UPDATE ";
    
    String b= tableName;
    
    String c =" set ";
    
    String d = attribute1+ "=?,";
    
    String e =attribute2+ "=?,";
    
    String f =attribute3+ "=?,";
    
    String g =attribute4+ "=?,";
    
    String h =attribute5+ "=?,";
    
    String i =attribute6+ "=?,";
    
    String j =attribute7+ "=?,";
    
    String k =attribute8+ "=?";
    
    String l = " where " +" "+ clauseAttribute + "=?";
    
    System.out.println(a+b+c+d+e+f+g+h+i+j+k+l); 
    
   PreparedStatement p= conn.prepareStatement(a+b+c+d+e+f+g+h+i+j+k+l);  
   
   
  //  p.setString(1, tableValue);   
    p.setString(1, attribute1Value );    
    p.setInt(2, attribute2Value);   
    p.setString(3,attribute3Value) ;
    p.setInt(4,attribute4Value) ;
    p.setInt(5,attribute5Value) ;
    p.setInt(6,attribute6Value) ;
    p.setInt(7,attribute7Value) ;
    p.setString(8,attribute8Value) ;
    p.setString(9,clauseAttributeValue);
    p.execute();  
//use execute if no results expected back   
  }catch(SQLException e){         System.out.println("Error"+e.toString());         
 }       catch (IllegalAccessException | ClassNotFoundException ex) { 
             Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
         }
  
  }
    
  //get answer
 // get possible ans
 
  //un comment to test
 /* public static void main(String[] args) throws ClassNotFoundException {
        // TODO code application logic here
         java.sql.Connection conn = null; 
//namanquah@ashesi.edu.gh 2015 3  
  
 System.out.println("This program demos DB connectivity");  
 
try {   Class.forName("com.mysql.jdbc.Driver").newInstance();  
  
  conn = java.sql.DriverManager.getConnection(    "jdbc:mysql://localhost/dyb?user=root&password=root");  
}  
catch (  ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) 
  
{   System.out.println(e);   System.exit(0);  } 

System.out.println("Connection established");

         try { 
            insert("user","id","pass","dob", "kevo","kevo",null);
         } catch (InstantiationException ex) {
                Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
         }
         
         System.out.println(authenticate("kevo","kevo","user"));
         
       //  addQuestion("1","cat","dog,cat,hey","what is cute?");
       
       System.out.println(getQuestionAttribute("1","question"));
       System.out.println(getQuestionAttribute("1","ans"));
       System.out.println(getQuestionAttribute("1","possible"));
 }*/
       
       
 public static void main(String[] args) throws ClassNotFoundException {
        // TODO code application logic here
         java.sql.Connection conn = null; 
//namanquah@ashesi.edu.gh 2015 3  
  
 System.out.println("This program demos DB connectivity");  
 
try {   Class.forName("com.mysql.jdbc.Driver").newInstance();  
  
  conn = java.sql.DriverManager.getConnection(    "jdbc:mysql://localhost/final?user=root&password=root");  
}  
catch (  ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) 
  
{   System.out.println(e);   System.exit(0);  } 

System.out.println("Connection establishedd");

try { 
            insert(
               
            "record5","name","number","adress",
            
            "qtychairs","qtytables","qtycanopies","duartion","dot",
            
            "t",2,"b",
            
            2,2,2,
            
            2,null
       );
         } catch (InstantiationException ex) {
                Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
         }

}
 
}